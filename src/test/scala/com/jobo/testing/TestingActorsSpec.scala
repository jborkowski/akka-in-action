package com.jobo.testing

import akka.actor.Actor.Receive
import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.testkit.TestActor.AutoPilot
import akka.testkit.{ImplicitSender, TestActor, TestActorRef, TestKit, TestProbe}
import com.jobo.basic.StopSystemAfterAll
import org.scalatest.{FlatSpecLike, MustMatchers}

class TestingActorsSpec extends TestKit(ActorSystem("testing-actors")) with FlatSpecLike with MustMatchers
  with StopSystemAfterAll with ImplicitSender {

  "Sync test" should " create TestActorRef" in {
    val sut = TestActorRef[ActorWithState]

    sut ! "one"
    sut ! "two"
    sut ! "three"


    sut.underlyingActor.state mustBe 3
  }
  "Sync test" should "use implicitSender" in {
    val sut = system.actorOf(Props[ActorWithState])

    sut ! "one"
    sut ! "two"
    sut ! "three"

    val request = sut ! "getState"

    expectMsgPF() {
      case state: Int => state mustBe 3
    }
  }

  it should "Use receiver while" in {
    val sutProbe = TestProbe()
    val sut = sutProbe.ref

    sut ! "one"
    sut ! "two"
    sut ! "three"
    sut ! "four"
    sut ! "five"

    import scala.concurrent.duration._
    val msg = sutProbe.receiveWhile(500 millis) {
      case msg: String if msg != "four" => msg
    }

    msg must contain only("one", "two", "three")
  }

  it should "use receive while" in {
    val sutProbe = TestProbe()
    val sut = sutProbe.ref

    sut ! "one"
    sut ! "two"
    sut ! "three"
    sut ! "four"
    sut ! "five"

    import scala.concurrent.duration._
    val messages=sutProbe.receiveWhile(500 millis){
      case message:String if message != "four" => message
    }

    messages must contain only("one","two","three")

  }

  it should "use receiveM " in {
    val sutProbe = TestProbe()
    val sut = sutProbe.ref

    sut ! "one"
    sut ! "two"
    sut ! "three"
    sut ! "four"
    sut ! "five"

    import scala.concurrent.duration._
    val messages: Seq[String] =sutProbe.receiveN(4,1 second).map(_.toString)

    messages must contain only("one","two","three","four")

  }


  "Test Probe" should "run with auto pilot" in {
    val probe1 = TestProbe()
    val probe2 = TestProbe()

    probe1.setAutoPilot(new AutoPilot {
      override def run(sender: ActorRef, msg: Any): AutoPilot = {
        probe2.ref.tell(msg,testActor)
        TestActor.KeepRunning
      }
    })

    probe2.setAutoPilot(new AutoPilot {
      override def run(sender: ActorRef, msg: Any): AutoPilot = {
        sender ! msg
        TestActor.KeepRunning
      }
    })

    probe1.ref ! "one"
    probe1.ref ! "two"
    probe1.ref ! "three"

    probe2.ref ! "four"
    probe2.ref ! "five"
    probe2.ref ! "six"

    val response: Seq[AnyRef] = receiveN(6)

    println(response)

    response must have size(6)
  }
}


class ActorWithState extends Actor {
  var state = 0
  override def receive: Receive = {
    case "getState" => sender ! state
    case _          => state = state + 1
  }
}