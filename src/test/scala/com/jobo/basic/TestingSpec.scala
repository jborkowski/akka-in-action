package com.jobo.basic

import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import com.jobo.basic.ExampleActor.{TestMessage, TestResponse}
import org.scalatest.{FlatSpecLike, MustMatchers}

class TestingSpec extends TestKit(ActorSystem("testing")) with MustMatchers with FlatSpecLike
  with StopSystemAfterAll with ImplicitSender {

  "Actor" should "send expected response" in {
    val other = TestProbe()

    val actor = system.actorOf(Props(new ExampleActor(other.ref)))

    actor ! TestMessage("aa")
    actor ! TestMessage("bbb")
    actor ! TestMessage("cccc")
    actor ! TestMessage("dddd")
    actor ! TestMessage("cccc")
    actor ! TestMessage("DdDdD")

    import scala.concurrent.duration._

    other.expectMsg(TestResponse(2))
    other.expectMsgPF() {
      case TestResponse(number) => number mustBe 3
    }

    val receivedNumbers = other. receiveWhile(500 millis) {
      case TestResponse(number) => number
    }
    receivedNumbers must contain theSameElementsInOrderAs(List(4,4,4,5))

    other.expectNoMsg(500 millis)
  }

}
