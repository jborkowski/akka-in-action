package com.jobo.basic

import org.scalatest.{FlatSpec, MustMatchers}

class BasicPartPatternMatchingSpec extends FlatSpec with MustMatchers {

  import PatternMatchingExercise._

  it should "properly match messages (Int,Int,String)" in {
    primaryExercise((8, 2, "+")) mustBe 10
    primaryExercise((3, 1, "+")) mustBe 4
    primaryExercise((8, 2, "-")) mustBe 6
    primaryExercise((3, 1, "-")) mustBe 2
    primaryExercise((8, 2, "*")) mustBe 16
    primaryExercise((3, 1, "*")) mustBe 3
  }

  it should "properly match case classes" in {
    additionalExercise(Number(7)) mustBe 7
    additionalExercise(Number(9)) mustBe 9
    additionalExercise(Add(Number(7), Number(2))) mustBe 9
    additionalExercise(Add(Number(7), Number(5))) mustBe 12
    additionalExercise(Mult(Number(7), Number(2))) mustBe 14
    additionalExercise(Mult(Number(7), Number(3))) mustBe 21
  }
}
