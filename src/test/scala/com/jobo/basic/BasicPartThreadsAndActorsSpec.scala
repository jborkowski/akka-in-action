package com.jobo.basic

import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import com.jobo.basic.ExerciseActor._
import org.scalatest.{FlatSpecLike, MustMatchers}

class BasicPartThreadsAndActorsSpec extends TestKit(ActorSystem("Thread")) with MustMatchers with FlatSpecLike
  with ImplicitSender with StopSystemAfterAll {

  "Actor" should "executing logic in different Thread" in {
    val actor = system.actorOf(Props[ExerciseActor])

    actor ! ThreadId
    val actorThreadId = expectMsgClass(classOf[Long])

    actor ! ThreadName
    val actorThreadName = expectMsgClass(classOf[String])

    println(s"Actor is working in [id: $actorThreadId, name: $actorThreadName]")
    println(s"Main thread is [id: $mainThreadId, name: $mainThreadName]")

    actorThreadId must not be equal(mainThreadId)
    actorThreadName must not be equal(mainThreadName)

  }

  "Actor" should "handle of request" in {
    val actor = system.actorOf(Props[ExerciseActor])

    actor ! List(ThreadId, ThreadName)

    val actorThreadId = expectMsgClass(classOf[Long])
    val actorThreadName = expectMsgClass(classOf[String])

    println(s"Actor is working in [id: $actorThreadId, name: $actorThreadName]")
    println(s"Main thread is [id: $mainThreadId, name: $mainThreadName]")

    actorThreadId must not be equal(mainThreadId)
    actorThreadName must not be equal(mainThreadName)
  }
  def mainThreadId = Thread.currentThread().getId
  def mainThreadName = Thread.currentThread().getName
}
