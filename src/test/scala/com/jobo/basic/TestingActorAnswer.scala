package com.jobo.basic

import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import com.jobo.basic.PingPongProtocol.{Ball, StartGame}
import org.scalatest.{FlatSpecLike, MustMatchers}

class TestingActorAnswer extends TestKit(ActorSystem("testing-actor-answering")) with MustMatchers with FlatSpecLike
  with StopSystemAfterAll with ImplicitSender {

  "ActorA" should "start playing when 'Start' is sent" in {
    val probe = TestProbe()
    val actorA = system.actorOf(Props(new ActorA(probe.ref, 3)))

    actorA ! StartGame
    actorA ! Ball(1)
    actorA ! Ball(2)
    actorA ! Ball(3)

    import scala.concurrent.duration._

    probe.expectMsg(Ball())
    probe.expectMsg(Ball(2))
    probe.expectMsg(Ball(3))

    probe.expectNoMsg(500 millis)
  }

  "ActorB" should "play for n rounds" in {
    val actorB = system.actorOf(Props(new ActorB(3)))

    actorB ! Ball(1)
    actorB ! Ball(2)
    actorB ! Ball(3)

    expectMsg(Ball(1))
    expectMsg(Ball(2))
    expectMsg(Ball(3))

    import scala.concurrent.duration._
    expectNoMsg(500 millis)
  }

}
