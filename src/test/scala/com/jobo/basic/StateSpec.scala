package com.jobo.basic

import akka.actor.Actor.Receive
import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import org.scalatest.{FlatSpecLike, MustMatchers}

class StateSpec extends TestKit(ActorSystem("Ohh")) with MustMatchers with FlatSpecLike
  with ImplicitSender with StopSystemAfterAll {

  import scala.concurrent.duration._

  "Duplicator" should "removes duplicates" in {
    val probe = TestProbe()

    val deduplicate = system.actorOf(Props(new Dedupicator(probe.ref)))

    deduplicate ! "MESSAGE_1"
    deduplicate ! "MESSAGE_1"
    deduplicate ! "MESSAGE_1"
    deduplicate ! "MESSAGE_2"
    deduplicate ! "MESSAGE_2"
    deduplicate ! "MESSAGE_4"
    deduplicate ! "MESSAGE_2"
    deduplicate ! "MESSAGE_5"

    probe.expectMsg("MESSAGE_1")
    probe.expectMsg("MESSAGE_2")
    probe.expectMsg("MESSAGE_4")
    probe.expectMsg("MESSAGE_5")

    probe.expectNoMsg(500 millis)
  }

  "Deduplicator -> WordProcessor combination" should "translate deduplicated messages" in {
      val probe=TestProbe()

      val wordProcessor=system.actorOf(Props(new WordProcessor(probe.ref)))
      val deduplicator = system.actorOf(Props(new Dedupicator(wordProcessor)))

      deduplicator ! "message1"
      deduplicator ! "message1"
      deduplicator ! "message2"
      deduplicator ! "message2"
      deduplicator ! "message2"
      deduplicator ! "Message3"

      probe.expectMsg("MESSAGE1")
      probe.expectMsg("MESSAGE2")
      probe.expectMsg("MESSAGE3")
      probe.expectNoMsg(500 millis)
    }
}

class Dedupicator(next: ActorRef) extends Actor {
  var directionary = Array[String]()

  override def receive: Receive = {
    case msg: String if !(directionary contains msg) => {
      directionary = directionary :+ msg
      next ! msg
    }
  }
}

class WordProcessor(next: ActorRef) extends Actor {
  override def receive: Receive = {
    case msg: String => next ! msg.toUpperCase
  }
}
