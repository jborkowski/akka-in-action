package com.jobo.basic

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest._

class SimpleActorSpec extends TestKit(ActorSystem("SimpleActor")) with FlatSpecLike
  with MustMatchers with StopSystemAfterAll with ImplicitSender {

  "SimpleActor for testing answer" should
    "return true if 'i' is 'even' and otherwise return false if number is 'odd'" in {
    //given
    val actor = system.actorOf(Props[SimpleActor])
    val even = 20
    val odd = 41

    //when
    actor ! even

    //then
    expectMsg(true)

    //when
    actor ! odd

    //then
    expectMsg(false)

  }

  it should "return sum of tuple" in {
    //given
    val actor = system.actorOf(Props[SimpleActor])

    val t1 = (3, 7)
    val t2 = (12, -1)

    //when
    actor ! t1

    //then
    expectMsg(10)

    //when
    actor ! t2

    //then
    expectMsg(11)

  }

  "Multiplying calculator" should "should multiply elements of received tuple" in {
    //given
    val calc: ActorRef = system.actorOf(Props[MultiplyActor])

    //when
    calc ! (3,5)
    calc ! (2,3)

    //then
    expectMsg(15)
    expectMsg(6)
  }
}
