package com.jobo.routing

import akka.actor.Actor.Receive
import akka.actor.{Actor, ActorRef, Props}

class CustomRouter(workersNumb: Int, workerProps: Props) extends Actor {

  val idxIterator: Iterator[Int] = Cycle.cycleBetween(0, workersNumb)

  def generatorWorkerName(workerNumber: Int) = {
    val parentName = self.path.name
    s"$parentName:worker:$workerNumber"
  }

  val workers: IndexedSeq[ActorRef] = {
    (0 to workersNumb).map(i => context.actorOf(workerProps, generatorWorkerName(i)))
  }

  override def receive: Receive = {
    case msg => workers(idxIterator.next()) forward msg
  }
}

object Cycle {
  def cycleBetween(start: Int, end: Int) = Iterator.iterate(start)(i => (i + 1)%end) //>>> ???
}

case class RoutedJob(toUpper:String)

class CustomWorker extends Actor {
  override def receive = {
    case RoutedJob(text) =>
      sender ! (text.toUpperCase + self.path.name)
  }
}