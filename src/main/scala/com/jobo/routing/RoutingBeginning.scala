package com.jobo.routing

import java.util.concurrent.TimeUnit

import akka.actor.Actor.Receive
import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.routing.{FromConfig, RandomGroup, RoundRobinPool}
import com.typesafe.config.ConfigFactory


object Ohh {
  def main(args: Array[String]): Unit = {
    println("*** Creating Routers ***")

    val config = ConfigFactory.load("routers/routes-beginning")
    val system = ActorSystem("routers", config)

    system.actorOf(Props[GroupSupervisor], "workers")

    println(" * Creating routers simulation")
    println(" * Test different routers in main actor receive method")
    val actor = system.actorOf(Props[MainActor], "mainActor")

    (1 to 10).foreach(i =>
      actor ! Work(s"value$i")
    )

    TimeUnit.MILLISECONDS.sleep(500)
    system.terminate()
  }
}

class MainActor extends Actor {

  val router1 = context.actorOf(FromConfig.props(Props[RouterWorker]), "routerNameFromConfig1")

  val router2 = context.actorOf(RoundRobinPool(3).props(Props[RouterWorker]), "router2")

  val group1 = context.actorOf(RandomGroup(
    List("/user/workers/groupWorker1", "/user/workers/groupWorker2")).props(), "router4")

  override def receive: Receive = {
    case work => router1 ! work
  }
}

case class Work(value: String)

class RouterWorker extends Actor {
  override def receive: Receive = {
    case Work(value) =>
      println(s"${self.path.toStringWithoutAddress} working on $value in ${Thread.currentThread().getName}")
  }
}

class GroupSupervisor extends Actor with ActorLogging {

  override def preStart(): Unit = {
    Seq(1,2).foreach { i =>
      val child = context.actorOf(Props[RouterWorker], s"groupWorker$i")
      log.info("created worker for group: {}", child.path)
    }
  }

  override def receive: Receive = {
    case msg =>
  }
}