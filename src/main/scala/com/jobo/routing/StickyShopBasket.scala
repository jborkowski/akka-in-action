package com.jobo.routing

import akka.actor.{Actor, ActorLogging}
import akka.routing.ConsistentHashingRouter.ConsistentHashMapping


object StickyShopBasket {

  sealed trait ShopActon

  final case class Purchase(product: ShopProductRouting) extends ShopActon

  final case object ListProduct extends ShopActon

  case class ShopProductRouting(name: String, price: BigDecimal)

  case class Request(sessionId: Int, action: ShopActon)

  case class Response[A](code: Int, content: A)

  object ShopBasket {
    def hashingFunction: ConsistentHashMapping = {
      case Request(sessionId, _) => sessionId
    }
  }

  class ShopBasket extends Actor with ActorLogging {
    type BasketItems = Seq[ShopProductRouting]
    var purchase = Map[Int, BasketItems]()

    override def receive = {
      case Request(sessionId, Purchase(product)) =>
        val currentBasket = purchase.getOrElse(sessionId, Seq.empty[ShopProductRouting])
        val newBasket = currentBasket :+ product
        purchase = purchase + (sessionId -> newBasket)
      case Request(sessionId, ListProduct) =>
        sender ! Response(200, purchase.get(sessionId))
    }
  }

}

