package com.jobo.routing
import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorSystem, Props}
import akka.routing.ConsistentHashingRouter.ConsistentHashable
import akka.routing.{Broadcast, ConsistentHashingPool}


object ConsistentHashing {
  def main(args: Array[String]): Unit = {
    val system = ActorSystem("hey-hashing")
    val router = system.actorOf(ConsistentHashingPool(3, virtualNodesFactor = 10).props(Props[HashingWorker]), "hashRouter")


    //HANDS ON - router with configured hashing strategy
    //    def routingStrategy:ConsistentHashMapping={
    //      case NormalMessage(id,_) => id
    //    }
    //    val router=system.actorOf(ConsistentHashingPool(3,virtualNodesFactor = 10,hashMapping = routingStrategy)
    //      .props(Props[HashingWorker]),"pureHashRouter")

    router ! HashedMessage(1, "Msg[1:1]")
    router ! HashedMessage(2, "Msg[2:18]")
    router ! HashedMessage(3, "Msg[3:17]")
    router ! HashedMessage(1, "Msg[4:15]")
    router ! HashedMessage(2, "Msg[5:14]")
    router ! HashedMessage(3, "Msg[7:11]")
    router ! HashedMessage(1, "Msg[2:6]")
    router ! HashedMessage(2, "Msg[3:3]")
    router ! HashedMessage(3, "Msg[1:7]")
    router ! HashedMessage(1, "Msg[3:5]")
    router ! HashedMessage(2, "Msg[6:2]")
    router ! HashedMessage(3, "Msg[12:0]")
    router ! Broadcast("DISPLAY")

    TimeUnit.MICROSECONDS.sleep(5000)
    system.terminate()
  }
}

case class NormalMessage(id: Int, content: String)
case class HashedMessage(id: Int, content: String) extends ConsistentHashable {
  override def consistentHashKey: Any = id
}

class HashingWorker extends Actor {
  var ids = Set[Int]()

  override def receive: Receive = {
    case msg: HashedMessage =>
      println(s"${self.path.toStringWithoutAddress} received Hashed Message with id : ${msg.id}: ${msg.content}")
      ids = ids + msg.id
    case msg: NormalMessage =>
      println(s"${self.path.toStringWithoutAddress} received Normal Message with id : ${msg.id} : ${msg.content}")
      ids = ids + msg.id
    case "DISPLAY" => println(s"${self.path.toStringWithoutAddress} has ids : ${ids}")
  }
}
