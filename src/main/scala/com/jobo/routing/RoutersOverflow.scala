package com.jobo.routing

import java.util.concurrent.TimeUnit

import akka.actor.Actor.Receive
import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import akka.routing.RoundRobinPool
import scala.concurrent.duration._

object Run {
  def main(args: Array[String]): Unit = {
    val system = ActorSystem("overflow")

    val destination = system.actorOf(RoundRobinPool(1).props(Props[Destination]))
    val source = system.actorOf(Props(new Source(destination)))


    source ! 'START
  }
}

  case class Msg(content: String)

  object Msg {
    val largeString: String = (1 to 100000).map(_.toChar).mkString("")

    def large = Msg(largeString + System.currentTimeMillis())
  }

  class Source(destination: ActorRef) extends Actor with ActorLogging {

    implicit val ec = context.dispatcher
    var sentMsg = 0

    override def receive: Receive = {
      case 'START =>
        log.info("Simulation Started")
        scheduleNext()
      case msg: Msg =>
        destination ! msg
        sentMsg = sentMsg + 1
        if (sentMsg % 100 == 0) log.info(s"SOURCE : sent $sentMsg messages")
        if (sentMsg == 1000) context.system.terminate()
        scheduleNext()
    }

    def scheduleNext(): Unit = {
      context.system.scheduler.scheduleOnce(1 millis) {
        self ! Msg.large
      }
    }
  }

  class Destination extends Actor with ActorLogging {
    var processedMsg = 0

    override def receive: Receive = {
      case msg =>
        TimeUnit.MILLISECONDS.sleep(50)
        processedMsg = processedMsg + 1
        if (processedMsg % 100 == 0) log.info(s"DESTINATION : processed $processedMsg message")
    }
  }

