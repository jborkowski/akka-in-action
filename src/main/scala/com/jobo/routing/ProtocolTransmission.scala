package com.jobo.routing

import java.nio.charset.Charset
import java.util.UUID

import akka.actor.{Actor, ActorLogging, ActorRef, Props}


object ProtocolTransmission {
  def main(args: Array[String]): Unit = {

  }


  class TxActor(receiver: ActorRef) extends Actor with ActorLogging {
    override def receive = {
      case SendWord(word) =>
        val id = UUID.randomUUID
        val len = word.length
        word.getBytes(Charset.defaultCharset)
            .zipWithIndex.map { case (s, i) => Frame(i, len ,s) }
            .map(item => SendByte(id, item))
            .foreach(receiver ! _)
    }
  }

  object RxActor {
    def props(processor: ActorRef) = Props(new RxActor(processor))
  }

  class RxActor(processor: ActorRef) extends Actor with ActorLogging {
    var received = Map[UUID, Seq[Frame]]()

    override def receive: Receive = {
      case SendByte(id, item) =>
        val tmp = received.getOrElse(id, Seq[Frame]())
        val updated = tmp :+ item
        received = received + (id -> updated)
        if (completeCheck(updated)) {
          self ! Show(id)
        }
      case Show(id) =>
        val msg = buildFromByte(received.getOrElse(id, Seq[Frame]()))
        log.info("Received MSG: {}", msg)
    }

    def completeCheck(data: Seq[Frame]): Boolean = {
      data.distinct.lengthCompare(data.last.total) == 0
    }

    def buildFromByte(data: Seq[Frame]): String = {
      val bytes = data.sortWith(_.idx < _.idx).map(_.value)
      new String(bytes.toArray, Charset.defaultCharset())
    }
  }

  case class Frame(idx: Int, total: Int, value: Byte)

  //MESSAGES
  case class SendWord(word: String)
  case class SendByte(id: UUID, item: Frame)

  case class Show(id: UUID)

}