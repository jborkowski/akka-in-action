package com.jobo.routing

import java.util.concurrent.TimeUnit

import akka.pattern.ask
import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.io.Udp.SO.Broadcast
import akka.util.Timeout



import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}

class CustomScatterGather {

}

//MESSAGES
case class Detect(word: String)
case class Detected(language: Option[String])
case class New(language: String, word: String)

class CustomScatterGatherRouter(workers: Seq[Props], timeout: Long = 1000) extends Actor with ActorLogging {
  private implicit val internalTimeout: Timeout = Timeout(timeout, TimeUnit.MICROSECONDS)
  private implicit val ec: ExecutionContextExecutor = context.dispatcher

  val pool: Seq[ActorRef] = workers.map(prop => context.actorOf(prop))

  override def receive = {
    case Broadcast(message) =>
      pool.foreach(_ ! message)
    case message =>
      val originSender = sender
      val futures = pool.map(_ ? message)
      val first = Future.firstCompletedOf(futures)
      first.onSuccess {
        case response => originSender ! response
      }
  }
}

class DictionaryWorker(sleepTime: Int = RandomSleep.sleep()) extends Actor with ActorLogging {
  import Dictionary._

  var dictionary: Map[String, Set[String]] = Map (
    "polish" -> Set("komputer", "przeglądarka", "klawiatura", "mysz", "programowanie"),
    "english" -> Set("computer", "browser", "keyboard", "mouse", "programming"),
    "esperanto" -> Set("komputilo", "retumilo", "klavaro", "muso", "programado")
  )

  override def receive: Receive = {
    case Detect(word) =>
      TimeUnit.MILLISECONDS.sleep(sleepTime)
      val detected = detect(dictionary)(word)
      sender ! Detected(detected)

    case New(language, word) =>
      dictionary = update(dictionary)(language, word)
  }
}

object Dictionary {
  type Dictionary = Map[String,Set[String]]

  def detect(dictionary: Dictionary)(word: String): Option[String] = {
    dictionary.find{
      case (_, set) => set.contains(word)
    }.map(_._1)
  }

  def update(dictionary: Dictionary)(language: String, word: String): Dictionary = {
    val words = dictionary.getOrElse(language, Set.empty[String])
    val newWords = words + word
    dictionary + (language -> newWords)
  }
}