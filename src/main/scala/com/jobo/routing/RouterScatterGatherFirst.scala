package com.jobo.routing

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.routing.ScatterGatherFirstCompletedPool

import scala.concurrent.duration._
import scala.util.Random

object Test {
  def main(args: Array[String]): Unit = {
    val system = ActorSystem("scatter-gather-first")
    val actorSender = system.actorOf(Props[Sender], "sender")
    actorSender ! 'START
    TimeUnit.SECONDS.sleep(5)
    system.terminate()
  }
}

case class MessageId(val v: Int) extends AnyVal
case class Request(id: MessageId)
case class Response(requestId: MessageId, from: String)

class Sender extends Actor with ActorLogging {

  val worker = context.actorOf(
    ScatterGatherFirstCompletedPool(3, within = 6 second).props(Props[Worker])
  )
  override def receive = {
    case 'START =>
      worker ! Request(MessageId(1))
      worker ! Request(MessageId(2))

    case Response(id, from) =>
      log.info(s"response $id received from $from")
  }
}

class Worker extends Actor with ActorLogging {
  override def receive = {
    case Request(id) =>
      val desctibeSelf = s"${self.path.toStringWithoutAddress}"
      val slept = RandomSleep.sleep()
      log.info(s"$desctibeSelf calculated $id for $slept")
      sender ! Response(id, desctibeSelf)
  }
}

object RandomSleep {
  private val periods: IndexedSeq[Int] = IndexedSeq(100, 500, 1000, 2000, 5000)
  def sleep(): Int = {
    val sleepMillis = periods(new Random().nextInt(5))
    TimeUnit.MILLISECONDS.sleep(sleepMillis)
    sleepMillis
  }
}