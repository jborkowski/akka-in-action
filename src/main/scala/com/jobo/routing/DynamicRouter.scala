package com.jobo.routing

import java.util.concurrent.TimeUnit

import akka.actor.Actor.Receive
import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.routing.{FromConfig, GetRoutees, Routees}
import com.typesafe.config.ConfigFactory


object DynamicRouter {
  def main(args: Array[String]): Unit = {
    val config = ConfigFactory.load("routers/routers-dynamic")
    val system = ActorSystem("dynamicRouting", config)

    val sender = system.actorOf(Props[Sender], "dynamicRouting")

    sender ! 'START

    TimeUnit.MILLISECONDS.sleep(29)
    system.terminate()
  }
}

class Sender extends Actor with ActorLogging {
  val workers = context.actorOf(FromConfig.props(Props[DynamicWorker]), "dynamicRouter")

  val tasks=List(10,10,10,10,10,10,10,100,10,10,2000,2000,200,10,10,10,10,10,10,10,2000,5000,5000)

  override def receive: Receive = {
    case 'START =>
      tasks.foreach { time =>
        log.info("Sending time: {}", time)
        workers ! time
        workers ! GetRoutees
        TimeUnit.MILLISECONDS.sleep(time)
      }
    case routers: Routees =>
      log.info("Current number of routes: {}", routers.routees.length)
  }
}

class DynamicWorker extends Actor {
  override def receive: Receive = {
    case _ => TimeUnit.MILLISECONDS.sleep(500)
  }
}