package com.jobo.routing

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.routing.FromConfig
import com.typesafe.config.ConfigFactory

object Ex {
  def main(args: Array[String]): Unit = {
    val config = ConfigFactory.load("routers/routes-ex")
    val systen = ActorSystem("routersExercise", config)

    val exerciseActor = systen.actorOf(Props[MathActor], "exerciseActor")
    val displayer = systen.actorOf(Props[Displayer], "displayer")

    exerciseActor.tell(Add(1 to 21), displayer)

    //group workers
    systen.actorOf(Props[ExerciseGroup], "exerciseGroup")

    exerciseActor.tell(Power(2,3), displayer)
    exerciseActor.tell(Power(2,4), displayer)
    exerciseActor.tell(Power(2,5), displayer)

    TimeUnit.MICROSECONDS.sleep(1100)
    systen.terminate()
  }
}


//MESSAGES
sealed trait MathOperation
case class Add(numbers: Range) extends MathOperation
case class Power(number: Int, power: Int) extends MathOperation

case class MathResponse(operation: MathOperation, result: Int)

class MathActor extends Actor with ActorLogging {

  val router = context.actorOf(FromConfig.props(Props[AddWorker]), "router1")
  val group = context.actorOf(FromConfig.props(Props[PowerWorker]), "group1")

  override def receive: Receive = {
    case add: Add => router forward add
    case power: Power => group forward power
  }
}

class AddWorker extends Actor with ActorLogging {
  override def receive = {
    case add @ Add(numbers) =>
      TimeUnit.MILLISECONDS.sleep(500)
      val result = numbers.sum
      sender ! MathResponse(add, result)
  }
}

class PowerWorker extends Actor with ActorLogging {

  @scala.throws[Exception](classOf[Exception])
  override def preStart(): Unit = log.info(s"starting PowerWorker ${self.path.toStringWithoutAddress}")

  override def receive = {
    case pow @ Power(number, power) =>
      TimeUnit.MICROSECONDS.sleep(500)
      val result = Math.pow(number, power)
      sender ! MathResponse(pow, result.toInt)
  }
}

class ExerciseGroup extends Actor with ActorLogging {

  @scala.throws[Exception](classOf[Exception])
  override def preStart(): Unit = {
    (1 to 3).foreach{ i =>
      context.actorOf(Props[PowerWorker], s"Worker$i")
    }
  }

  override def receive: Receive = {
    case msg =>
      throw new RuntimeException(s"ExerciseGroup shouldn't receive msg ;)")
  }
}

class Displayer extends Actor {
  override def receive: Receive = {
    case msg => println(s"received $msg from ${sender.path.toStringWithoutAddress}")
  }
}