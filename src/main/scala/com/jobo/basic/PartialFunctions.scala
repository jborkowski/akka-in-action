package com.jobo.basic

object PartialFunctions {
  val head = (l: List[Int]) => l.head

  val partialHead = new PartialFunction[List[Int],Int] {
    override def isDefinedAt(x: List[Int]): Boolean = x.length > 0
    override def apply(v1: List[Int]): Int = v1.head
  }

  val partialHeadPM:PartialFunction[List[Int],Int] = {
    case head::_ => head
  }

  lazy val add:PartialFunction[(Int,Int,String), Int] = {
    case (x, y, "+") => x + y
  }

  lazy val mult:PartialFunction[(Int,Int,String), Int] = {
    case (x, y, "*") => x * y
  }

  val calc = add orElse mult

  def main(args: Array[String]): Unit = {
    println("PARTIAL FUNCTIONS DEMO")

    println("\n    * partial head Pattern Matching List(1,2,3) : " + partialHeadPM.isDefinedAt(List(1,2,3)))
    println("    * partial head Pattern Matching List() : " + partialHeadPM.isDefinedAt(List()))
    println("    * partial head Pattern Matching List(1,2,3) : " + partialHeadPM(List(1,2,3)))

    println("\n calc " + calc (12, 42, "*"))

  }
}
