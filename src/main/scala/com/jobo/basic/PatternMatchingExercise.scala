package com.jobo.basic

object PatternMatchingExercise {
  def primaryExercise(instruction: (Int, Int, String)) : Int = instruction match {
    case (a, b, "+") => a + b
    case (a, b, "*") => a * b
    case (a, b, "-") => a - b
  }

  sealed trait Expression

  case class Number(v: Int) extends Expression

  case class Add(n1: Number, n2: Number) extends Expression

  case class Mult(n1: Number, n2: Number) extends Expression

  def additionalExercise(e: Expression) : Int = e match {
    case Number(x) => x
    case Add(Number(x),Number(y)) => x + y
    case Mult(Number(x),Number(y)) => x * y
  }

}
