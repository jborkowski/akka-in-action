package com.jobo.basic

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}

import scala.concurrent.Await
import scala.concurrent.duration.Duration

object State {
  def main(args: Array[String]): Unit = {
    val system = ActorSystem("oooh")
    val history = system.actorOf(Props[History])
    val forwarder = system.actorOf(Props(new Forwarder(history)))

    forwarder ! "MSG1"
    forwarder ! "MSG2"
    TimeUnit.MILLISECONDS.sleep(400)
    forwarder ! "MSG3"
    forwarder ! "MSG4"

    TimeUnit.MILLISECONDS.sleep(300)
    history ! 'DISPLAY

    system.terminate()
    Await.result(system.whenTerminated, Duration(2, TimeUnit.SECONDS))
  }
}


class Forwarder(actorRef: ActorRef) extends Actor {
  override def receive: Receive = {
    case msg: String  => actorRef ! s"[message: $msg, time: ${Clock.time}]"
    case unhandled    => println(s"Unhandled message $unhandled")
  }
}

class History extends Actor with ActorLogging {
  var history = Vector[String]()

  override def receive = {
    case 'DISPLAY     => log.info(history.mkString("\n","\n","\n"))
    case msg: String  => history = history :+ msg
  }
}


object Clock {
  def time: String = {
    val formatter = new SimpleDateFormat("hh:mm:ss.SSS")
    formatter.format(Calendar.getInstance().getTime)
  }
}