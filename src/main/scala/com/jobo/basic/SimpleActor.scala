package com.jobo.basic

import akka.actor.Actor.Receive
import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, PoisonPill, Props}
import com.jobo.basic.ExampleActor.{TestMessage, TestResponse}
import com.jobo.basic.PingPongProtocol.{Ball, StartGame}

object Application extends App {
  override def main(args: Array[String]) {
    val system = ActorSystem("basic")
    val actorProperties = Props[SimpleActor]
    val simpleActorInstance: ActorRef = system.actorOf(actorProperties)

    simpleActorInstance ! "Hello! "
  }
}



class SimpleActor extends Actor {
  override def receive: Receive = {
    case i: Int => {
      if (i % 2 == 0) {
        sender ! true
      } else {
        sender ! false
      }
    }

    case (v1: Int, v2: Int) => sender ! v1 + v2

    case _ => println("this catches all incoming messages")
  }
}

class MultiplyActor extends Actor {
  override def receive: Receive = {
    case (v1: Int, v2: Int) => sender ! v1 * v2
  }
}

object ExerciseActor {
  case object ThreadId
  case object ThreadName
}

class ExerciseActor extends Actor {
  import ExerciseActor._

  override def receive: Receive = {
    case ThreadId => sender ! Thread.currentThread().getId
    case ThreadName => sender ! Thread.currentThread().getName
    case l: List[_] => l.foreach { i => receive(i) }
  }
}

object ExampleActor {
  case class TestMessage(text: String)
  case class TestResponse(len: Int)
}

class ExampleActor(other: ActorRef) extends Actor {
  override def receive = {
    case example @ TestMessage(text) => other ! TestResponse(text.length)
  }
}

object PingPongProtocol {
  case object StartGame
  case class Ball(round:Int = 1)
}

class ActorA(player: ActorRef, limit: Int) extends Actor with ActorLogging {
  override def receive: Receive = {
    case StartGame => player ! Ball()
    case Ball(round) if round < limit => player ! Ball(round + 1)
    case _ =>
      log.info("ActorA stop playing")
      self ! PoisonPill
  }
}

class ActorB(limit: Int) extends Actor with ActorLogging {
  var roundsRemain = limit

  override def receive: Receive = {
    case b: Ball if(roundsRemain>0) => sender ! b
    case _: Ball =>
      log.info("ActorB stop playing")
      self ! PoisonPill
  }
}