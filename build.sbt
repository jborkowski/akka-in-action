name := "akka-in-action"

version := "1.0"

scalaVersion := "2.11.8"

organization := "com.jobo"

lazy val akkaVersion = "2.4.12"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % "test",
  "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % "test",
  "com.typesafe.akka" %  "akka-typed-experimental_2.11" % "2.4.11",
  "org.typelevel"     %% "cats" % "0.8.0",
  "org.scalatest"     %% "scalatest" % "3.0.0" % "test"
)

parallelExecution in Test := false